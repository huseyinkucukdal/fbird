﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;


public class Bgmove : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    float itemWidth;
    void Start()
    {
        var item = GetComponent<Renderer>();
        itemWidth = item.bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = (Vector2)transform.position + Vector2.left * speed * Time.deltaTime;
        if(transform.position.x <= -itemWidth){
            transform.position = (Vector2)transform.position + Vector2.right * itemWidth * 2f;
        }
    }
}
